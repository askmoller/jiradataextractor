﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace JiraDataExtractor.UI.Modules.Utilities
{
    /// <summary>
    /// Base view model for all view models
    /// </summary>
    public class ViewModelBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Sets a property value and raise the propertyChanged event handler.
        /// </summary>
        /// <typeparam name="T">The property type</typeparam>
        /// <param name="storage">The property backing/storage field</param>
        /// <param name="value">The new value</param>
        /// <param name="propertyName">The properly name</param>
        /// <returns>True if a new value is set. False if the new value equals the old value</returns>
        protected bool SetValue<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            RaisePropertyChanged(propertyName);
            return true;
        }
        /// <summary>
        /// Property changed event handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Invoke the PropertyChanged event handler
        /// Also leverages .net 4.5 CallerMemberName attribute
        /// </summary>
        /// <param name="propertyName"></param>
        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
