﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using Atlassian.Jira;
using JiraDataExtractor.Domain.Common.Interfaces;
using JiraDataExtractor.Domain.Common.Services;
using JiraDataExtractor.Domain.Common.Settings;
using JiraDataExtractor.UI.Modules.Services;
using JiraDataExtractor.UI.Modules.Utilities;

namespace JiraDataExtractor.UI.Modules.MainWindow
{
    public class MainWindowViewModel : ViewModelBase
    {
        private readonly IMessageBox _messageBox;
        private bool _isUIEnabled = true;
        private bool _errorOccurred;
        private string _userName;
        private string _statusMsg;
        private SolidColorBrush _statusMsgBackground;
        private string _customQueryInformation = "Selected query information";

        public MainWindowViewModel(IFileDialog fileDialog, IMessageBox messageBox, UserPreferences userPreferences, JiraRestService jiraRestService, ExcelWriteService excelWriteService)
        {
            _messageBox = messageBox;
            UserPreferences = userPreferences;
            JiraRestService = jiraRestService;
            ExcelWriteService = excelWriteService;
            UserName = userPreferences.LoginSettings.UserName;
            BrowserSavePathCommand = new RelayCommand(_ => SavePath = fileDialog.OpenFolderDialog(UserPreferences.SaveDataSettings.SavePath, "Select folder to save files"));
            ExecuteCommand = new RelayCommand(async (x) => await Execute(x));
            OpenSaveDirectoryCommand = new RelayCommand(_ => fileDialog.OpenSaveDirectory());
            CustomQueryOptionsCommand = new RelayCommand(_ => throw new NotImplementedException());
            userPreferences.SaveDataSettings.SaveToExcel = true;
        }

        public UserPreferences UserPreferences { get; }
        public JiraRestService JiraRestService { get; }
        public ExcelWriteService ExcelWriteService { get; }
        public string Title => "Jira Data Extractor";

        public bool IsUIEnabled
        {
            get { return _isUIEnabled; }
            set { SetValue(ref _isUIEnabled, value); }
        }
        public string JiraUrl
        {
            get { return UserPreferences.LoginSettings.Url; }
            set
            {
                if (value != UserPreferences.LoginSettings.Url)
                {
                    UserPreferences.LoginSettings.Url = value;
                    RaisePropertyChanged();
                }
            }
        }
        public string UserName
        {
            get { return _userName; }
            set { SetValue(ref _userName, value); }
        }
        public bool RememberUserAndPass
        {
            get { return UserPreferences.LoginSettings.RememberUserAndPass; }
            set
            {
                if (value != UserPreferences.LoginSettings.RememberUserAndPass)
                {
                    UserPreferences.LoginSettings.RememberUserAndPass = value;
                    RaisePropertyChanged();
                }
            }
        }
        public string TeamName
        {
            get { return UserPreferences.QuerySettings.TeamName; }
            set
            {
                if (value != UserPreferences.QuerySettings.TeamName)
                {
                    UserPreferences.QuerySettings.TeamName = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool LoggedHours
        {
            get { return UserPreferences.QuerySettings.LoggedHours; }
            set
            {
                if (value != UserPreferences.QuerySettings.LoggedHours)
                {
                    UserPreferences.QuerySettings.LoggedHours = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool IsStartTimeEnabled
        {
            get { return UserPreferences.QuerySettings.IsStartTimeEnabled; }
            set
            {
                if (value != UserPreferences.QuerySettings.IsStartTimeEnabled)
                {
                    UserPreferences.QuerySettings.IsStartTimeEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool IsEndTimeEnabled
        {
            get { return UserPreferences.QuerySettings.IsEndTimeEnabled; }
            set
            {
                if (value != UserPreferences.QuerySettings.IsEndTimeEnabled)
                {
                    UserPreferences.QuerySettings.IsEndTimeEnabled = value;
                    RaisePropertyChanged();
                }
            }
        }
        public DateTime StartTime
        {
            get { return UserPreferences.QuerySettings.StartTime; }
            set
            {
                if (value != UserPreferences.QuerySettings.StartTime)
                {
                    UserPreferences.QuerySettings.StartTime = value;
                    RaisePropertyChanged();
                }
            }
        }
        public DateTime EndTime
        {
            get { return UserPreferences.QuerySettings.EndTime; }
            set
            {
                if (value != UserPreferences.QuerySettings.EndTime)
                {
                    UserPreferences.QuerySettings.EndTime = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool LastSprint
        {
            get { return UserPreferences.QuerySettings.LastSprint; }
            set
            {
                if (value != UserPreferences.QuerySettings.LastSprint)
                {
                    UserPreferences.QuerySettings.LastSprint = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool OpenSprints
        {
            get { return UserPreferences.QuerySettings.OpenSprints; }
            set
            {
                if (value != UserPreferences.QuerySettings.OpenSprints)
                {
                    UserPreferences.QuerySettings.OpenSprints = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool AllSprints
        {
            get { return UserPreferences.QuerySettings.AllSprints; }
            set
            {
                if (value != UserPreferences.QuerySettings.AllSprints)
                {
                    UserPreferences.QuerySettings.AllSprints = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool UseCustomQuery
        {
            get { return UserPreferences.QuerySettings.UseCustomQuery; }
            set
            {
                if (value != UserPreferences.QuerySettings.UseCustomQuery)
                {
                    UserPreferences.QuerySettings.UseCustomQuery = value;
                    RaisePropertyChanged();
                }
            }
        }
        public string CustomQueryInformation
        {
            get { return _customQueryInformation; }
            set { SetValue(ref _customQueryInformation, value); }
        }
        public string SavePath
        {
            get { return UserPreferences.SaveDataSettings.SavePath; }
            set
            {
                if (value != UserPreferences.SaveDataSettings.SavePath)
                {
                    UserPreferences.SaveDataSettings.SavePath = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool SaveToCSV
        {
            get { return UserPreferences.SaveDataSettings.SaveToCSV; }
            set
            {
                if (value != UserPreferences.SaveDataSettings.SaveToCSV)
                {
                    UserPreferences.SaveDataSettings.SaveToCSV = value;
                    RaisePropertyChanged();
                }
            }
        }
        public bool SaveToExcel
        {
            get { return UserPreferences.SaveDataSettings.SaveToExcel; }
            set
            {
                if (value != UserPreferences.SaveDataSettings.SaveToExcel)
                {
                    UserPreferences.SaveDataSettings.SaveToExcel = value;
                    RaisePropertyChanged();
                }
            }
        }
        public string StatusMsg
        {
            get { return _statusMsg; }
            set { SetValue(ref _statusMsg, value); }
        }
        public SolidColorBrush StatusMsgBackground
        {
            get { return _statusMsgBackground; }
            set { SetValue(ref _statusMsgBackground, value); }
        }
        public ICommand BrowserSavePathCommand { get; }
        public ICommand CustomQueryOptionsCommand { get; }
        public ICommand OpenSaveDirectoryCommand { get; }
        public ICommand ExecuteCommand { get; }
        private async Task Execute(object parameter)
        {
            IsUIEnabled = false;
            var passwordBox = parameter as System.Windows.Controls.PasswordBox;
            var password = passwordBox?.Password ?? string.Empty;

            StatusMsgBackground = Brushes.Transparent;
            _errorOccurred = false;

            SaveUserPreferences(password);
            var jiraConnection = await CreateJiraConnection(password);
            var loggedHours = await GetJiraIssues(() => JiraRestService.GetLoggedHours(jiraConnection), "logged hours issues", LoggedHours);
            var openSprints = await GetJiraIssues(() => JiraRestService.GetOpenSprints(jiraConnection), "issues in open sprints", OpenSprints);
            var lastSprint = await GetJiraIssues(() => JiraRestService.GetLastSprint(jiraConnection), "issues in last sprint", LastSprint);

            var epics = await GetJiraIssues(() => JiraRestService.GetEpics(jiraConnection), "epics");

            await PrintIssues(() => ExcelWriteService.SaveLoggedHours(loggedHours, epics), "logged hours", LoggedHours);
            await PrintIssues(() => ExcelWriteService.SaveOpenSprints(openSprints, epics), "open sprints", OpenSprints);
            await PrintIssues(() => ExcelWriteService.SaveLastSprint(lastSprint, epics), "last sprint", LastSprint);

            if (!_errorOccurred)
                SetStatusMessage("Executed successful", success: true);

            IsUIEnabled = true;
        }
        private void SetStatusMessage(string message, bool failed = false, bool success = false)
        {
            StatusMsgBackground = success ? Brushes.GreenYellow : failed ? Brushes.Red : Brushes.Transparent;
            StatusMsg = message;
        }
        private async Task ShowProgress(Task task, string message)
        {
            await Task.Run(() =>
            {
                var loadingText = "";
                while (!task.IsCompleted && !task.IsFaulted && !task.IsCanceled)
                {
                    loadingText += " .";
                    if (loadingText.Length > 40)
                        loadingText = "";

                    SetStatusMessage($"{message} {loadingText}");
                    Thread.Sleep(100);
                }
            });
        }
        private void ShowError(Exception ex, string message)
        {
            SetStatusMessage(message, failed: true);
            _messageBox.ShowException(this, ex?.InnerException ?? ex ?? new Exception(), System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(message));
            _errorOccurred = true;
        }
        public void OnWindowClosing(System.Windows.Controls.PasswordBox passwordBox)
        {
            var password = passwordBox?.Password ?? string.Empty;
            SaveUserPreferences(password);
        }
        public void SaveUserPreferences(string password)
        {
            if (RememberUserAndPass)
            {
                UserPreferences.LoginSettings.UserName = UserName;
                UserPreferences.LoginSettings.Password = password;
            }
            else
            {
                UserPreferences.LoginSettings.UserName = default;
                UserPreferences.LoginSettings.Password = default;
            }

            try
            {
                SetStatusMessage("saving data");
                Serializer.Save(UserPreferences, ConfigurationProvider.SettingsFilePath);
            }
            catch (Exception ex)
            {
                ShowError(ex, "Saving data failed");
            }
        }
        private async Task<Jira> CreateJiraConnection(string password)
        {
            if (_errorOccurred) return null;
            try
            {
                var task = Task.Run(() => JiraRestService.CreateJiraConnection(UserName, password));
                await ShowProgress(task, "logging in. Please wait");
                if (task.IsFaulted || task.IsCanceled)
                    throw new Exception();
                return task.Result;
            }
            catch (Exception)
            {
                ShowError(new ArgumentException($"Please check the following:\n\n" +
                                                $"- Access to the internet\n" +
                                                $"- Jira instance are running and accessible: {UserPreferences.LoginSettings.Url}\n" +
                                                $"- User name and password are correct"),
                    "log in error");
            }
            return null;
        }
        private async Task<IPagedQueryResult<Issue>> GetJiraIssues(Func<Task<IPagedQueryResult<Issue>>> issueTask, string msg, bool isChecked = true)
        {
            if (_errorOccurred || !isChecked) return null;
            try
            {
                var task = issueTask.Invoke();
                await ShowProgress(task, $"Getting {msg}. Please wait");
                return task.Result;
            }
            catch (Exception ex)
            {
                ShowError(ex, $"failed getting {msg}");
            }
            return null;
        }
        private async Task PrintIssues(Action saveAction, string msg, bool isChecked)
        {
            if (_errorOccurred || !isChecked) return;
            try
            {
                var task = Task.Run(saveAction);
                await ShowProgress(task, $"Saving {msg} to excel. Please wait");
                if (task.IsFaulted || task.IsCanceled)
                    ShowError(task.Exception, $"failed saving {msg} to excel");
            }
            catch (Exception ex)
            {
                ShowError(ex, $"failed saving {msg} to excel");
            }
        }
    }
}
