﻿using System.Windows;

namespace JiraDataExtractor.UI.Modules.MainWindow
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        public MainWindowView(MainWindowViewModel viewModel)
        {
            InitializeComponent();
            Closing += MainWindowView_Closing;

            DataContext = viewModel;
            passwordBox.Password = viewModel.UserPreferences.LoginSettings.Password;
        }

        private void MainWindowView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var viewModel = (MainWindowViewModel)DataContext;
            viewModel.OnWindowClosing(passwordBox);
        }
    }
}
