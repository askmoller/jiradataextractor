﻿using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using JiraDataExtractor.Domain.Common.Settings;

namespace JiraDataExtractor.UI.Modules.Services
{
    public class JiraRestService
    {
        //private const string NotOpenOrReady = "status != Open AND status != Ready";
        private const string ClosedOrInProgress = "(status = Closed OR status = Refine OR status = Resolved OR status = Review OR status = \"In Progress\")";

        public JiraRestService(UserPreferences userPreferences)
        {
            UserPreferences = userPreferences;
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        }
        public UserPreferences UserPreferences { get; }
        public Jira CreateJiraConnection(string userName, string password)
        {
            var loginSettings = UserPreferences.LoginSettings;
            var jiraConnection = Jira.CreateRestClient(loginSettings.Url, userName, password);
            jiraConnection.Issues.MaxIssuesPerRequest = 100;

            // This is a "hack" to check if login can be done successfully. If the users credentials are wrong an exception is thrown.
            var query = new IssueSearchOptions("issue = GMCI-1") { StartAt = 0, MaxIssuesPerRequest = 1 };
            jiraConnection.Issues.GetIssuesFromJqlAsync(query).Wait();
            
            return jiraConnection;
        }

        public async Task<IPagedQueryResult<Issue>> GetLoggedHours(Jira jiraConnection)
        {
            var jql = new StringBuilder($"project = GMCI AND \"Assigned Team\" = {UserPreferences.QuerySettings.TeamName} AND issuetype != Epic AND {ClosedOrInProgress}");
            if (UserPreferences.QuerySettings.IsStartTimeEnabled)
                jql.Append($" AND status changed to \"In Progress\" after \"{UserPreferences.QuerySettings.StartTime:yyyy/MM/dd HH:mm}\"");
            if (UserPreferences.QuerySettings.IsEndTimeEnabled)
                jql.Append($" AND status changed to closed before \"{UserPreferences.QuerySettings.EndTime:yyyy/MM/dd HH:mm}\"");

            var query = new IssueSearchOptions(jql.ToString())
            {
                StartAt = 0,
                MaxIssuesPerRequest = 999,
            };
            return await jiraConnection.Issues.GetIssuesFromJqlAsync(query);
        }
        public async Task<IPagedQueryResult<Issue>> GetLastSprint(Jira jiraConnection)
        {
            // There does not seem to be a direct way to get the latest closed sprint.
            // The approach here is to get all the issues within the last month. Then by looking at the custom fields for sprint we might be able to find the last closed sprint
            var jql = $"project = GMCI AND \"Assigned Team\" = {UserPreferences.QuerySettings.TeamName} AND issuetype != Epic AND status changed to closed after \"{DateTime.Today.AddMonths(-1):yyyy/MM/dd HH:mm}\"";
            var query = new IssueSearchOptions(jql)
            {
                StartAt = 0,
                MaxIssuesPerRequest = 999,
            };
            //TODO Missing logic to filer query for issues in last closed sprint
            return await jiraConnection.Issues.GetIssuesFromJqlAsync(query);
        }
        public async Task<IPagedQueryResult<Issue>> GetOpenSprints(Jira jiraConnection)
        {
            var jql = $"project = GMCI AND \"Assigned Team\" = {UserPreferences.QuerySettings.TeamName} AND Sprint in openSprints()";
            var query = new IssueSearchOptions(jql)
            {
                StartAt = 0,
                MaxIssuesPerRequest = 999,
            };
            return await jiraConnection.Issues.GetIssuesFromJqlAsync(query);
        }
        public async Task<IPagedQueryResult<Issue>> GetEpics(Jira jiraConnection)
        {
            var jql = $"project = GMCI AND \"Assigned Team\" = {UserPreferences.QuerySettings.TeamName} AND issuetype = Epic";
            var query = new IssueSearchOptions(jql)
            {
                StartAt = 0,
                MaxIssuesPerRequest = 999,
            };
            return await jiraConnection.Issues.GetIssuesFromJqlAsync(query);
        }
    }
}