﻿using System;
using System.Collections.Generic;
using System.Linq;
using Atlassian.Jira;

namespace JiraRestApi
{
    public class JiraRequest_Legacy
    {
        public JiraRequest_Legacy()
        {
            var url = "https://jira.kitenet.com/";
            var userName = "";
            var password = "";
            var maxIssuesPerRequest = 999;
            var jiraConn = CreateJiraConnection(url, userName, password, maxIssuesPerRequest);


            CreateAndPrintReportToConsole(jiraConn, "R1.6", "team_404", "Sprint 7");
        }

        public void OtherProgram()
        {

            var jiraConn = Jira.CreateRestClient("http://jira.kitenet.com/", "omjirauser", "E8rcZ40tQmI32g");
            jiraConn.Issues.MaxIssuesPerRequest = 9999;

            var bugs = from i in jiraConn.Issues.Queryable
                       where
                                (i.Project == "DGXV"
                                && i.Type == "Bug"
                                && (i.Status == "Open" || i.Status == "In Progress" || i.Status == "Reopened" || i.Status == "Refine" || i.Status == "Ready" || i.Status == "Need Info"))
                       select i;

            var stories = from i in jiraConn.Issues.Queryable
                          where
                                   (i.Project == "DGXV"
                                   && i.Type == "Story"
                                   && (i.Status == "Open" || i.Status == "In Progress" || i.Status == "Reopened" || i.Status == "Refine" || i.Status == "Ready" || i.Status == "Need Info"))
                          select i;

            var bugList = bugs.ToList();
            var storyList = stories.ToList();

            //var filteredBugs = bugList.Where(x => x.FixVersions.Any(y => y.Name == args[1]));
            //var filteredStories = storyList.Where(x => x.FixVersions.Any(y => y.Name == args[1]));
            //var dateTime = DateTime.Now;

            //var bugsAndStoriesInJira = new List<string>()
            //{
            //    $"{dateTime};{filteredBugs.Count()};{filteredStories.Count()}" //Date;bugs;stories
            //};

            //bugsAndStoriesInJira.ForEach(Console.WriteLine);
            //File.AppendAllLines(args.Length > 0 ? args[0] : "JiraBugAndStoryLog.txt", bugsAndStoriesInJira);
        }

        public Jira CreateJiraConnection(string url, string userName, string password, int maxIssuesPerRequest)
        {
            var jiraConn = Jira.CreateRestClient(url, userName, password);
            jiraConn.Issues.MaxIssuesPerRequest = maxIssuesPerRequest;
            return jiraConn;
        }
        public void CreateAndPrintReportToConsole(Jira jiraConn, string releaseVersion, string assignedTeam, string sprint)
        {
            PrintReportInfo(releaseVersion, assignedTeam, sprint);

            GenerateSprintReportAndPrintToConsole(jiraConn, releaseVersion, assignedTeam, sprint);
        }

        private static void PrintReportInfo(string releaseVersion, string assignedTeam, string sprint)
        {
            Console.WriteLine("Release: {0}", releaseVersion);
            Console.WriteLine("Team: {0}", assignedTeam);
            Console.WriteLine("Sprint: {0}", sprint);
            Console.WriteLine();
        }

        private void GenerateSprintReportAndPrintToConsole(Jira jiraConn, string releaseVersion, string assignedTeam, string sprint)
        {
            //Completed
            var completedIssues = GetAllCompletedIssues(jiraConn, releaseVersion, assignedTeam, sprint);
            PrintToConsole("Completed", completedIssues, true);

            //Not Completed
            var totalNotCompletedIssues = GetAllNotCompletedIssues(jiraConn, releaseVersion, assignedTeam, sprint);
            PrintToConsole("Not Completed", totalNotCompletedIssues, true);

            //In Review
            var issuesInReview = totalNotCompletedIssues.Select(x => x).Where(x => x.Status == "Resolved").ToList();
            PrintToConsole("In Review", issuesInReview, true);
        }
        private List<Issue> GetAllCompletedIssues(Jira jiraConn, string releaseVersion = null, string assignedTeam = null, string sprint = null)
        {
            var closedIssues = from issue in jiraConn.Issues.Queryable
                               where issue.Project == "GMCI" &&
                                     (issue.Type == "Task" || issue.Type == "Bug") &&
                                     (issue.Status == "Closed")
                               select issue;
            var listOfClosedIssues = closedIssues.ToList();
            var issues = listOfClosedIssues;
            if (releaseVersion != null)
                issues = GetIssuesPerRelease(issues, releaseVersion);
            if (assignedTeam != null)
                issues = GetIssuesPerTeam(issues, assignedTeam);
            if (sprint != null)
                issues = GetIssuesPerSprint(issues, sprint);

            return issues;
        }

        private List<Issue> GetAllNotCompletedIssues(Jira jiraConn, string releaseVersion = null, string assignedTeam = null, string sprint = null)
        {
            var totalOpenIssues = from issue in jiraConn.Issues.Queryable
                                  where issue.Project == "GMCI" &&
                                        (issue.Type == "Task" || issue.Type == "Bug") &&
                                        (issue.Status == "Open" || issue.Status == "In Progress" || issue.Status == "Reopened" || issue.Status == "Refine" || issue.Status == "Ready" || issue.Status == "Need Info" || issue.Status == "Resolved")
                                  select issue;

            var issues = totalOpenIssues.ToList();

            if (releaseVersion != null)
                issues = GetIssuesPerRelease(issues, releaseVersion);

            if (assignedTeam != null)
                issues = GetIssuesPerTeam(issues, assignedTeam);
            if (sprint != null)
                issues = GetIssuesPerSprint(issues, sprint);

            return issues;
        }

        private List<Issue> GetIssuesPerSprint(List<Issue> issues, string sprint)
        {
            var issuesPerSprint = issues.Select(x => x)
                .Where(x => x.CustomFields.Any(p => p.Name == "Sprint" &&
                                                    p.Values.Any(v => v.Contains(sprint))
                )).ToList();
            return issuesPerSprint;
        }

        private List<Issue> GetIssuesPerTeam(List<Issue> issues, string assignedTeam)
        {
            var issuesPerTeam = issues.Select(x => x).Where(x => x.CustomFields.Any(p =>
                p.Name == "Assigned Team" &&
                p.Values[0] == assignedTeam)).ToList();

            return issuesPerTeam;
        }

        private List<Issue> GetIssuesPerRelease(List<Issue> issues, string releaseVersion)
        {
            var issuesPerRelease = issues.Select(x => x).Where(x => x.FixVersions.Any(v => v.Name.Contains(releaseVersion))).ToList();
            return issuesPerRelease;
        }

        private void PrintToConsole(string label, List<Issue> issues, bool printTotalStoryPoints = false)
        {
            Console.WriteLine();
            Console.WriteLine("{0}", label);

            int i = 1;//just for numbering..
            foreach (var issue in issues)
            {
                var storyPoints = issue.CustomFields.Select(x => x).Where(x => x.Name.Contains("Story Points"))?.FirstOrDefault()?.Values[0];
                string values = String.Format("{0, -5}\t{1,-10}\t\tPoints: {2, -10}\t{3, -5}\t{4, -15}\t{5, -15}\t{6}", //align to Left
                    i++,
                    issue.Key,
                    storyPoints,
                    issue.Type,
                    issue.Priority,
                    issue.Status,
                    issue.Summary
                );
                Console.WriteLine(values);
            }
            var totalStoryPoints = GetTotalStoryPoints(issues);

            if (printTotalStoryPoints)
                Console.WriteLine("Total Story Points: {0}", totalStoryPoints);

            Console.WriteLine();
        }

        private int GetTotalStoryPoints(List<Issue> issues)
        {
            if (issues == null || issues.Count == 0) return 0;

            int totalStoryPoints = 0;
            foreach (var issue in issues)
            {
                var storyPointsStr = issue.CustomFields.Select(x => x).Where(x => x.Name.Contains("Story Points"))?.FirstOrDefault()?.Values[0];

                int storyPoints;
                if (int.TryParse(storyPointsStr, out storyPoints))
                {
                    totalStoryPoints += storyPoints;
                }
            }
            return totalStoryPoints;
        }
    }
}
