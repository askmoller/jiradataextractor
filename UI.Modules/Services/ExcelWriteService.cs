﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Atlassian.Jira;
using JiraDataExtractor.Domain.Common.Settings;
using Microsoft.Office.Interop.Excel;

namespace JiraDataExtractor.UI.Modules.Services
{
    public class ExcelWriteService
    {
        private readonly object _missing = System.Reflection.Missing.Value;
        public ExcelWriteService(UserPreferences userPreferences)
        {
            UserPreferences = userPreferences;
            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        }
        public UserPreferences UserPreferences { get; }
        public void SaveLoggedHours(IEnumerable<Issue> issues, IEnumerable<Issue> epics)
        {
            //excel  Changes
            int i = 2;

            _Application excel = new Application {DisplayAlerts = false};

            var path = UserPreferences.SaveDataSettings.SavePath;
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            var fileName = $"LoggedHours {CultureInfo.CurrentCulture.TextInfo.ToTitleCase(UserPreferences.QuerySettings.TeamName)} {DateTime.Now:yyyy-MM-dd HH.mm.ss}";
            var fullPath = Path.Combine(path, fileName + ".xlsx");
            _Workbook workbook = excel.Workbooks.Add("");
            _Worksheet worksheet = string.IsNullOrEmpty("Sheet1") ? (_Worksheet)workbook.ActiveSheet : (_Worksheet)workbook.Worksheets["Sheet1"];
            
            worksheet.Cells[1, 1] = "Name";
            worksheet.Cells[1, 2] = "Key";
            worksheet.Cells[1, 3] = "Summary";
            worksheet.Cells[1, 4] = "Status";
            worksheet.Cells[1, 5] = "ResolutionDate";
            worksheet.Cells[1, 6] = "TimeSpent";
            worksheet.Cells[1, 7] = "StoryPoints";
            foreach (var issue in issues)
            {
                var storyPointsCustomField = issue.CustomFields.FirstOrDefault(x => x.Name == "Story Points");
                var storyPoints = storyPointsCustomField != null ? storyPointsCustomField.Values[0] : "";

                var epicLinkCustomField = issue.CustomFields.FirstOrDefault(x => x.Name == "Epic Link");
                var epicLink = epicLinkCustomField != null ? epicLinkCustomField.Values[0] : "";

                worksheet.Cells[i, 1] = issue.Type.Name;
                worksheet.Cells[i, 2] = issue.Key;
                worksheet.Cells[i, 3] = issue.Summary;
                worksheet.Cells[i, 4] = issue.Status;
                worksheet.Cells[i, 5] = issue.ResolutionDate;
                worksheet.Cells[i, 6] = issue.TimeTrackingData.TimeSpent;
                worksheet.Cells[i, 7] = storyPoints;
                i++;

            }
            worksheet.Range["A1", "G1"].EntireColumn.AutoFit();
            worksheet.Columns.VerticalAlignment = XlVAlign.xlVAlignCenter;
            var uRange = worksheet.UsedRange;
            uRange.Worksheet.ListObjects.Add(XlListObjectSourceType.xlSrcRange, uRange, _missing, XlYesNoGuess.xlYes, _missing).Name = "Table1";
            uRange.Select();
            uRange.Worksheet.ListObjects["Table1"].TableStyle = "TableStyleMedium6";
            workbook.SaveAs(fullPath, _missing, _missing, _missing, _missing, _missing, XlSaveAsAccessMode.xlExclusive, _missing, _missing, _missing, _missing, _missing);
            workbook.Close();
            excel.Quit();
        }
        public void SaveLastSprint(IEnumerable<Issue> issues, IEnumerable<Issue> epics)
        {
        }
        public void SaveOpenSprints(IEnumerable<Issue> issues, IEnumerable<Issue> epics)
        {
        }
    }
}