﻿using System.IO;
using JiraDataExtractor.Domain.Common.Interfaces;
using JiraDataExtractor.Domain.Common.Settings;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace JiraDataExtractor.UI.Modules.Services
{
    public class FileDialogService : IFileDialog
    {
        public FileDialogService(UserPreferences userPreferences)
        {
            UserPreferences = userPreferences;
        }
        public UserPreferences UserPreferences { get; }
        public string OpenFolderDialog(string initialFolder, string dialogTitle)
        {
            var openFolderDialog = new CommonOpenFileDialog
            {
                Title = dialogTitle,
                IsFolderPicker = true,
                InitialDirectory = initialFolder
            };
            var dialogResult = openFolderDialog.ShowDialog();
            return dialogResult == CommonFileDialogResult.Ok ? openFolderDialog.FileName : initialFolder;
        }

        public void OpenSaveDirectory()
        {
            var path = UserPreferences.SaveDataSettings.SavePath;
            if (!Directory.Exists(path))
            {
                // If the folder don't exist then open the parent folder
                path = Directory.GetParent(path).FullName;
                if (!Directory.Exists(path))
                    return;
            }
            System.Diagnostics.Process.Start(new System.Diagnostics.ProcessStartInfo()
            {
                FileName = path,
                UseShellExecute = true,
                Verb = "open"
            });
        }
    }
}