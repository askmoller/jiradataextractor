﻿using System;
using System.Windows;
using JiraDataExtractor.Domain.Common.Interfaces;

namespace JiraDataExtractor.UI.Modules.Services
{
    public class MessageBoxService : IMessageBox
    {
        public void Show(string message)
        {
            MessageBox.Show(message, "", MessageBoxButton.OK);
        }

        public void Show(string message, string caption)
        {
            MessageBox.Show(message, caption, MessageBoxButton.OK);
        }
        public void ShowException(object sender, Exception e)
        {
            MessageBox.Show(e.Message + "\n\n" + e.StackTrace, $"{sender} Error!", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public void ShowException(object sender, Exception e, string caption)
        {
            MessageBox.Show(e.Message + "\n\n" + e.StackTrace, caption, MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }
}
