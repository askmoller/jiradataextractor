﻿namespace JiraDataExtractor.Domain.Common.Interfaces
{
    public interface IFileDialog
    {
        string OpenFolderDialog(string initialFolder, string dialogTitle);
        void OpenSaveDirectory();
    }
}
