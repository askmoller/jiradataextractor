﻿using System;

namespace JiraDataExtractor.Domain.Common.Interfaces
{
    public interface IMessageBox
    {
        void Show(string message);
        void Show(string message, string caption);
        void ShowException(object sender, Exception e);
        void ShowException(object sender, Exception e, string caption);
    }
}
