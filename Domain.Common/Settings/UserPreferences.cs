﻿namespace JiraDataExtractor.Domain.Common.Settings
{
    public class UserPreferences
    {
        public LoginSettings LoginSettings { get; set; } = new LoginSettings();
        public QuerySettings QuerySettings { get; set; } = new QuerySettings();
        public SaveDataSettings SaveDataSettings { get; set; } = new SaveDataSettings();
    }
}