﻿namespace JiraDataExtractor.Domain.Common.Settings
{
    public class LoginSettings
    {
        public string Url { get; set; } = "https://jira.kitenet.com/";
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool RememberUserAndPass { get; set; }
    }
}