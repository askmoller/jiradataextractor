﻿using System;
using System.IO;

namespace JiraDataExtractor.Domain.Common.Settings
{
    public class SaveDataSettings
    {
        public bool SaveToCSV { get; set; }
        public bool SaveToExcel { get; set; }
        public string SavePath { get; set; } = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "JiraDataExtractor");
    }
}