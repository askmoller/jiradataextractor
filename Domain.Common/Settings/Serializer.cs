﻿using System.IO;
using System.Xml.Serialization;

namespace JiraDataExtractor.Domain.Common.Settings
{
    public class Serializer
    {
        public static void Save<T>(T objectToSave, string path)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            using (StreamWriter myWriter = new StreamWriter(path, false))
            {
                mySerializer.Serialize(myWriter, objectToSave);
            }

        }
        public static T Load<T>(string path)
        {
            XmlSerializer mySerializer = new XmlSerializer(typeof(T));
            using (FileStream myFileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                return (T)mySerializer.Deserialize(myFileStream);
            }
        }
    }
}
