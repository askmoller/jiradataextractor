﻿namespace JiraDataExtractor.Domain.Common.Settings
{
    public class Query
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public bool IsEnable { get; set; }
    }
}