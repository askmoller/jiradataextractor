﻿using System;
using System.Collections.Generic;

namespace JiraDataExtractor.Domain.Common.Settings
{
    public class QuerySettings
    {
        public string TeamName { get; set; }
        public bool LoggedHours { get; set; }
        public bool IsStartTimeEnabled { get; set; }
        public bool IsEndTimeEnabled { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool OpenSprints { get; set; }
        public bool LastSprint { get; set; }
        public bool AllSprints { get; set; }
        public bool UseCustomQuery { get; set; }
        public List<Query> CustomQueries { get; set; }
    }
}
