﻿using System;
using System.IO;
using JiraDataExtractor.Domain.Common.Interfaces;
using JiraDataExtractor.Domain.Common.Settings;

namespace JiraDataExtractor.Domain.Common.Services
{
    public class ConfigurationProvider
    {
        private static readonly string SettingsFolderPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "JiraDataExtractor");
        private const string SettingsFileName = "settings.xml";
        private readonly IMessageBox _exception;
        public ConfigurationProvider(IMessageBox exception)
        {
            _exception = exception;
        }
        public static string SettingsFilePath => Path.Combine(SettingsFolderPath, SettingsFileName);
        public UserPreferences GetConfiguration()
        {
            try
            {
                if (File.Exists(SettingsFilePath))
                {
                    return Serializer.Load<UserPreferences>(SettingsFilePath);
                }
                else
                {
                    var settings = new UserPreferences();
                    if (!Directory.Exists(SettingsFolderPath))
                    {
                        Directory.CreateDirectory(SettingsFolderPath);
                    }
                    Serializer.Save(settings, SettingsFilePath);
                    return settings;
                }
            }
            catch (Exception ex)
            {
                _exception.ShowException(this, ex);
                var settings = new UserPreferences();
                // TODO Consider refactor
                try
                {
                    if (!Directory.Exists(SettingsFolderPath))
                    {
                        Directory.CreateDirectory(SettingsFolderPath);
                    }
                    Serializer.Save(settings, SettingsFilePath);
                }
                catch (Exception ee)
                {
                    _exception.ShowException(this, ee);
                }
                return settings;
            }
        }
    }
}