﻿using System.Threading;
using System.Windows;
using JiraDataExtractor.Domain.Common.Interfaces;
using JiraDataExtractor.Domain.Common.Services;
using JiraDataExtractor.UI.Modules.MainWindow;
using JiraDataExtractor.UI.Modules.Services;
using Unity;

namespace JiraDataExtractor.UI.Shell
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static Mutex _mutex;
        /// <summary>
        /// This is the starting point of the application. It creates the IoC container and sets up the the necessary controls and services.
        /// </summary>
        /// <param name="e"></param>
        protected override void OnStartup(StartupEventArgs e)
        {
            // This block is to ensure that only 1 instance of this application are running
            _mutex = new Mutex(true, "JiraDataExtractor:4344AFEA-788B-41DC-9A17-2F58BA8E407E", out bool createdNew);
            if (!createdNew)
            {
                Application.Current.Shutdown();
            }

            base.OnStartup(e);

            var container = new UnityContainer();

#if DEBUG
            container.AddExtension(new Diagnostic());
#endif
            
            container.RegisterInstance(container);

            container.RegisterType<IFileDialog, FileDialogService>();
            container.RegisterType<IMessageBox, MessageBoxService>();
            container.RegisterType<JiraRestService>();

            var configurationProvider = container.Resolve<ConfigurationProvider>();

            // Register a run-time instance of the UserPreferences. This will be shared among the services.
            container.RegisterInstance(configurationProvider.GetConfiguration());

            // Alternative implementation: The UserPreferences will be loaded from the ConfigurationProvider everytime it is injected into a view/control or programatically resolved.
            //container.RegisterFactory<UserPreferences>(_ => configurationProvider.GetConfiguration());
            // For more info about unity container see: https://www.tutorialsteacher.com/ioc/register-and-resolve-in-unity-container

            // Show the UI
            container.Resolve<MainWindowView>().Show();
        }
    }
}
